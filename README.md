# Docker/podman containers for various tools

- `geteltorito` [[src](geteltorito)][[hub](https://hub.docker.com/r/yakshaveinc/geteltorito)] - utility to create USB image from (Lenovo) ISO (to update my BIOS to send public report to vLLM project)

## Container image layout convention

The image format follows [`Whalebrew` standard](https://github.com/whalebrew/whalebrew?tab=readme-ov-file#creating-packages).
```
WORKDIR /workdir

ENTRYPOINT ["/path/to/binary"]
```

## TODO

- [ ] [Discord/Telegram integration](https://gitlab.com/yakshaveinc/containers/-/settings/integrations)
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [x] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)

## Contributing

- Issues: https://gitlab.com/yakshaveinc/containers/-/issues
- MRs: https://gitlab.com/yakshaveinc/containers/-/merge_requests

## Authors and acknowledgment

- @abitrolly

(add yourself)

## License

Public domain.

## Project status

Trying to automate everything to spend more time on something else.
